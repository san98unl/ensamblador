;Examen de Ensamblador
;Por: Santiago Tuqueres
; 24-08-2020
;Operaciones de Numeros,Loop,Pilay cola, jmp

;*********Macros**************
%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro			; finaliza la instruccion repetitiva "imprimir"
%macro leer 1
	mov eax, 3 					;define el tipo de operacion
	mov	ebx, 2					;estandar de entrada
	mov ecx, %1		; lo que captura el teclado
	mov edx, 2					; numero de caracteres que se necesitanc on el enter
	int 80h
%endmacro
;******************************
section .data
     mensaje db 'Ingrese primer numero ',10
	len_mensaje equ $-mensaje
	
	mensaje2 db 'Ingrese segundo numero ',10
	len_mensaje2 equ $-mensaje2
	
    resultado db "El resultado es:",10
    len equ $-resultado

    new_line db " ", 10
    len_new_line equ $-new_line

    prueba db "yo",10
    len_prueba equ $-prueba

section .bss
    a resb 1
    b resb 1
	c resb 1
   
section .text
	global _start

_start:
    ;**********primer y tercer operando*****
    mov al, 2 ; se representa en codigo ascii
    add al,'0'
    mov [a], al
    mov rcx,1
   



ciclo:
    
    push rcx
     ;#primer operando
    mov ax,[a]
    ;#segundi operandi
    add rcx,'0'
    mov [b],rcx
    ;#tercer operando operando
    sub rcx,'0'
    sub rax,'0'
    mul rcx
    add rax, '0'    ; ajuste
    
    mov [c],rax


    ;imprimir a,1
    ;imprimir msj1,len_msj1
    ;imprimir b,1
    ;imprimir msj2,len_msj2
    imprimir c,1
    imprimir new_line,len_new_line
    pop rcx
    inc rcx
    cmp cx,5; mayor a 10
    jnz ciclo;
    imprimir new_line,len_new_line


    mov eax,1
    int 80h