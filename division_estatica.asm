; Division de 2 numeros Estaticos, no se ingresan por teclado
; Por: Santiago Tuqueres
section .data
    resultado db "El resultado es:",10
    len equ $-resultado

    resultado_residuo db "El residuo es:",10
    len_res equ $-resultado_residuo
    new_line db " ", 10
    len_new_line equ $-new_line
    

section .bss
    division resb 1 ; reservame 5 espacios o caracteres
    residuo resb 1
    cociente resb 1

section .text
    global _start
_start:
;**********imprimir*****
    mov al, 9     ; se representa en codigo ascii
    mov bh, 3     ; se representa en codigo ascii
    div bh   
    add al, '0'    ; ajuste
    mov [cociente],al
    add ah, '0'
    mov [residuo],ah
    

    mov eax, 4
    mov ebx, 1
    mov ecx, resultado
    mov edx, len
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, cociente
    mov edx, 1
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, new_line
    mov edx, len_new_line
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, resultado_residuo
    mov edx, len_res
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, residuo
    mov edx, 1
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, new_line
    mov edx, len_new_line
    int 80h
    
    mov eax,1
    int 80h
