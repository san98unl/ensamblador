; Suma de 2 numeros Estaticos, no se ingresan por teclado
%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro			; finaliza la instruccion repetitiva "imprimir"

section .data
    resultado db "El resultado es:",10
    len equ $-resultado
    new_line db " ", 10
    len_new_line equ $-new_line

    resultado2 db "El resultado de la resta es:",10
    len2 equ $-resultado2

    resultado3 db "El resultado de la multiplicacion es:",10
    len3 equ $-resultado3

    resultado4 db "El resultado del cociente es:",10
    len4 equ $-resultado4

    resultado5 db "El resultado del residuo es:",10
    len5 equ $-resultado5
    
    
section .bss
    suma resb 1 ; reservame 5 espacios o caracteres
     
    resta resb 1 ; reservame 5 espacios o caracteres
    multiplicacion resb 1 ; reservame 5 espacios o caracteres

    division resb 1 ; reservame 5 espacios o caracteres
    cociente_div resb 1 ; reservame 5 espacios o caracteres
    residuo_div resb 1 ; reservame 5 espacios o caracteres
    
    

section .text
    global _start
_start:
;**********imprimir*****
    mov eax, 4     ; se representa en codigo ascii
    mov ebx, 2      ; se representa en codigo ascii
    add eax, ebx    ; eax= eax + ebx
    add eax, '0'    ; ajuste
    mov [suma],eax

    imprimir resultado, len

    mov eax, 4
    mov ebx, 1
    mov ecx, suma
    mov edx, 1
    int 80h

    imprimir new_line, len_new_line


;*************Resta***
    mov eax, 4     ; se representa en codigo ascii
    mov ebx, 2      ; se representa en codigo ascii
    sub eax, ebx    ; eax= eax + ebx
    add eax, '0'    ; ajuste
    mov [resta],eax

    imprimir resultado2, len2

    mov eax, 4
    mov ebx, 1
    mov ecx, resta
    mov edx, 1
    int 80h

    imprimir new_line, len_new_line

;*************Mul***
    mov eax, 4     ; se representa en codigo ascii
    mov ebx, 2      ; se representa en codigo ascii
    mul  ebx    ; eax= eax + ebx
    add eax, '0'    ; ajuste
    mov [multiplicacion],eax

    imprimir resultado3, len3

    mov eax, 4
    mov ebx, 1
    mov ecx, multiplicacion
    mov edx, 1
    int 80h

    imprimir new_line, len_new_line
;************division******
    mov al, 4    ; se representa en codigo ascii
    mov bh, 2     ; se representa en codigo ascii
    div bh   
    add al, '0'    ; ajuste
    mov [cociente_div],al
    add ah, '0'
    mov [residuo_div],ah
    

    imprimir resultado4, len4

    mov eax, 4
    mov ebx, 1
    mov ecx, cociente_div
    mov edx, 1
    int 80h

    imprimir new_line, len_new_line

    imprimir resultado5, len5

    mov eax, 4
    mov ebx, 1
    mov ecx, residuo_div
    mov edx, 1
    int 80h

    imprimir new_line, len_new_line

    mov eax,1
    int 80h