%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro	
section	.data

	asterisco db '*'  ; para presentar el asterisco


	new_line db 10,''  ; para el salto de linea

section	.text

   global _start 

_start:	

     
    mov rcx,10
    mov rbx,1

l1:
    push rcx
    push rbx
    ;*****Nueva linea *****8
    
    
    ;mov rbx,rcx
;
    ;imprimir new_line,1
    ;pop rcx
    ;push rcx

l2:
    push rcx

     ;*****asterisco *****8
    imprimir asterisco,1
     
    pop rcx 
    loop l2

    imprimir new_line,1
    ;****** final loop columnas
    pop rbx
    pop rcx
    inc rbx
    
    loop l1 
    
    mov rax,1
    int 80h