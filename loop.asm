%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro			; finaliza la instruccion repetitiva "imprimir"

section .data
	mensaje db 'Ingrese un numero ',10
	len_mensaje equ $-mensaje
	
	mensaje2 db 'Ingrese un 2 numero ',10
	len_mensaje2 equ $-mensaje2
	
	mensaje_presentacion db 'El numero ingresado es : ',10
	len_presentacion equ $-mensaje_presentacion
	
section .bss
	a resb 1 
	b resb 1
	resultado resb 1
	

section .text
	global _start
_start:
	;****************************imprime mensaje 1******************
    mov eax,4
	mov ebx, 1
	mov ecx, mensaje
	mov edx, len_mensaje
	int 80h
	;leer primer valor
	mov eax, 3 					;define el tipo de operacion
	mov	ebx, 2					;estandar de entrada
	mov ecx, a		; lo que captura el teclado
	mov edx, 2					; numero de caracteres que se necesitanc on el enter
	int 80h
	
	;****************************imprime mensaje  2******************
	mov eax,4
	mov ebx, 1
	mov ecx, mensaje2
	mov edx, len_mensaje2
	int 80h
	
	;leer segundo valor
	mov eax, 3 					;define el tipo de operacion
	mov	ebx, 2					;estandar de entrada
	mov ecx, b					; lo que captura el teclado
	mov edx, 5					; numero de caracteres
	int 80h
   ;*************Potencia*******
   |

	;****************************salir del sistema******************
	mov eax,1
	int 80h