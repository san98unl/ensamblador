;Calculadora de saltos cin opciones infinita
; Por Santiago Tuqueres
; 13-07-2020
%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro			; finaliza la instruccion repetitiva "imprimir"

section .data
	msg1		db		10,'-Calculadora-',10,0
	lmsg1		equ		$ - msg1
 
	msg2		db		10,'Numero 1: ',0
	lmsg2		equ		$ - msg2
 
	msg3		db		'Numero 2: ',0
	lmsg3		equ		$ - msg3

	msg4		db		'1. Sumar: ',10,0
	lmsg4		equ		$ - msg4
	
	msg5		db		'2. Restar: ',10,0
	lmsg5		equ		$ - msg5

	msg6		db		'3. Multiplicar: ',10,0
	lmsg6		equ		$ - msg6

	msg7		db		'4. Division: ',10,0
	lmsg7		equ		$ - msg7

	msg8		db		'Seleccione la opcion',10,0
	lmsg8		equ		$ - msg8

	msg9		db		10,'Resultado: ',0
	lmsg9		equ		$ - msg9
 
	msg10		db		10,'Opcion Invalida',10,0
	lmsg10		equ		$ - msg10

	msg11		db		'5. Salir ',10,0
	lmsg11		equ		$ - msg11
 
	nlinea		db		10,10,0
	lnlinea		equ		$ - nlinea
 
section .bss
	opcion:		resb 	2
  	num1:		resb	2
	num2:		resb 	2
	resultado:	resb 	2
    contador:   resb    2
 
section .text
 	global _start
 
_start:
	;*********Titulo o encabezado del ejercicio***
	; Imprimimos en pantalla el mensaje 1
	imprimir msg1, lmsg1
 
	; Imprimimos en pantalla el mensaje del primer numero
	imprimir msg2, lmsg2
 
	; Obtenemos el numero 1
	mov eax, 3
	mov ebx, 0
	mov ecx, num1
	mov edx, 2
	int 80h
	;**********mensaje de segundo numero
	; Imprimimos en pantalla el mensaje 3
	imprimir msg3, lmsg3
 
	; Obtenemos el numero 2
	mov eax, 3
	mov ebx, 0
	mov ecx, num2
	mov edx, 2
	int 80h
	;**mensajes del menu
    menu:
    imprimir nlinea, lnlinea
	imprimir msg4, lmsg4; mensaje sumar

	imprimir msg5, lmsg5; mensaje restar

	imprimir msg6, lmsg6; mensaje multiplicar

	imprimir msg7, lmsg7; mensaje dividir

	imprimir msg11, lmsg11; mensaje salir

	;****mensaje de opcion****
	imprimir msg8, lmsg8

	mov eax, 3; camturar la opcion
	mov ebx, 0
	mov ecx, opcion
	mov edx, 2
	int 80h

	mov ah, [opcion]
	sub ah, '0'

	;comparar el valoringresad por el usuario, para saber que operacion se quiere realizar
	; JE= JMP(condicional) IF exist iquality, salta cuando los dos valores son iguales
	;
	
	cmp ah,1
	JE sumar

	cmp ah,2
	JE restar

	cmp ah,3
	JE multiplicar
	
	cmp ah,4
	JE dividir

	cmp ah,5
	JE salir


	;jmp dividir  
 
	; Si el valor ingresado no cumple con ninguna de las condiciones anteriores entonces mostramos un mensaje de error y finalizamos
	; la ejecucion del programa
	imprimir msg10, lmsg10
	jmp salir
 
 sumar:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Sumamos el registro AL y BL
	add al, bl
;	aaa
 
	; Convertimos el resultado de la suma de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al
 
	; Imprimimos en pantalla el mensaje 9
	imprimir msg9, lmsg9
 
	; Imprimimos en pantalla el resultado
	imprimir resultado, 2
	jmp menu
	;jmp restar

 
restar:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Restamos el registro AL y BL
	sub al, bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al
 
	; Imprimimos en pantalla el mensaje 9
	imprimir msg9, lmsg9
 
	; Imprimimos en pantalla el resultado
	imprimir resultado, 1
 	jmp menu
	;jmp multiplicar

 
multiplicar:
 
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Multiplicamos. AX = AL X BL
	mul bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], ax
 
	; Imprimimos en pantalla el mensaje 9
	imprimir msg9, lmsg9
 
	; Imprimimos en pantalla el resultado
	imprimir resultado, 1
 
	jmp menu

 
dividir:
 
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Igualamos a cero los registros DX y AH
	mov dx, 0
	mov ah, 0
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Division. AL = AX / BL. AX = AH:AL
	div bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], ax
 
	; Print on screen the message 9
	imprimir msg9, lmsg9
 
	; Imprimimos en pantalla el resultado
	imprimir resultado, 1
	jmp menu
	;jmp sumar

 
salir:
; Imprimimos en pantalla dos nuevas lineas
	imprimir nlinea, lnlinea
 
	; Finalizamos el programa
	mov eax, 1
	mov ebx, 0
	int 80h
