%macro escribir 2
	mov eax,4  
	mov ebx,1
	mov ecx,%1
	mov edx,%2
	int 80h
%endmacro

%macro leer 2
	mov eax,3  
	mov ebx,2
	mov ecx,%1
	mov edx,%2
	int 80h
%endmacro

section .data
    msj db 'Ingrese un numero: ', 10
    len equ $-msj

section .bss
    n1 resb 1

section .text
    global _start

_start:
    escribir msj, len
    leer n1, 2 

    mov eax, [n1]
    sub eax, '0'
    jmp principal

principal:
    
    cmp eax, 0
    jz salir ;o... jr se activa solo si hay cero
    jmp imprimir

imprimir:
    add eax, '0'
    push rax
    escribir eax, 1 
    pop rax
    sub eax, '0'
    dec eax
    jmp principal

salir:
    mov eax, 1
    int 80h