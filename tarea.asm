;Comentar el significado de cada línea

%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro			; finaliza la instruccion repetitiva "imprimir"

%macro leer 2		; inicia una instruccion repetitiva denominada "leer" que se puede llamar en cualquier parte
    mov eax,3		; indica el tipo de operacion en este caso sera de lectura  
    mov ebx,0		; indica tipo de entrada en este caso es entrada estandar
    mov ecx,%1 		; puntero a un area de memoria donde se dejaran los caracteres obtenidos que estan en el primer operando
    mov edx,%2		; muestra el numero de caracteres a leer que se encuentran en el segundo operando
    int 80H 		; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro			; finaliza la instruccion repetitiva "leer"


; ecx,modo de acceso
; edx, permisos
section .bss			; seccion donde se declaran variables estaticas 
	auxiliar resb 30	; define un espacio de memoria etiquetado como "auxiliar" de 30 bytes sin valor inicial
	auxiliarb resb 30	; define un espacio de memoria etiquetado como "auxiliarb" de 30 bytes sin valor inicial
	auxiliarc resb 30	; define un espacio de memoria etiquetado como "auxiliarc" de 30 bytes sin valor inicial


section .data				; seccion donde se declaran las constantes del programa
	msg db 0x1b ,"       " ; 6 espacios para contener al dato
	lenmsg equ $-msg		; constante "lenmsg" donde se calcula el numero de caracteres que tiene el dato "msg"



	salto db " ",10 		; define un byte de memoria de la constante etiquetada como "salto" con valor inicial del codigo ASCII para " " y se hace un salto de linea
	lensalto equ $-salto	; constante denominada "lensalto" que calcula el numero de caracteres de la constante "salto"




section .text				; declara la seccion donde se ejecuta el codigo
    global _start    		; directiva que exporta simbolos en el codigo

_start:						; punto de entrada del codigo ejecutable
	
	mov ecx,9				; almacena el 9 al registro ECX

	mov al,0				; almacena el 0 al registro al
	mov [auxiliar],al		; copia al en un byte en auxiliar

cicloI:						; seccion en la que se inicia un cicloI que realiza operaciones de forma secuencial
	push ecx				; inserta una palabra doble en Cx y decrementa el valor de la pila por 4
	mov ecx,9				; almacena el valor 9 en el registro ECX

	mov al,0				; almacena el 0 al registro al
	mov [auxiliarb],al		; copia al en un byte en auxiliar

	cicloJ:					; inicia un bloque de Instrucciones denominado "cicloJ"	
		push ecx			; inserta una decrementa el valor de la pila por 4


		call imprimir0al9	; llama a la instruccion "imprimir0al9" y despues retorna al origen
		

	;	imprimir msg2,lenmsg2

	fincicloJ:				; inicia un bloque de Instrucciones denominado "fincicloJ"	
		mov al,[auxiliarb]	; copia el byte que esta en "auxiliarb" en al
		inc al				; incrementa el registro AL en uno
		mov [auxiliarb],al	; copia AL en el byte en auxiliarb

		pop ecx				; quita el valor de ECX en la pila y luego añade 4 a ESP 
		loop cicloJ			; repite el cicloJ el numero de veces que esta registrado en CX
		
	;imprimir salto,lensalto

fincicloI:					; inicia un bloque de Instrucciones denominado "fincicloI"
	mov al,[auxiliar]		; copia el byte que esta en "auxiliar" en al
	inc al					; incrementa el registro AL en uno
	mov [auxiliar],al		; copia AL en el byte en auxiliar

	pop ecx					; quita el valor de ECX en la pila y luego añade 4 a ESP
	loop cicloI				; repite el cicloJ el numero de veces que esta registrado en CX
	

salir:						; inicia un bloque de Instrucciones denominado "salir"
	mov eax, 1				; realiza salida del proceso actual y retorno al sistema que lo invocó
	int 80H				    ; interrupcion de software libre linux y da significado a las anteriores lineas



imprimir0al9:				; inicia un bloque de Instrucciones denominado "imprimir0al9"
	
	mov ebx,"["				; almacena el valor del codigo ASCII para "[" en EBX
	mov [msg+1], ebx		; copia el registro EBX en el byte actual de msg con un desplazamiento de 1 

	mov bl,[auxiliar]		; copia el byte que esta en "auxiliar" en BL
	add bl,'0'				; el valor de BL sera igual a la suma del ultimo valor de BL + codigo ASCII de '0'
	mov [msg+2], bl			; copia el registro EBX en el byte actual de msg con un desplazamiento de 2


	mov ebx,";"				; almaceno el caracter ";" en codigo ASCII en EBX
	mov [msg+3], ebx		; copia el registro EBX en el byte actual de msg con un desplazamiento de 3

	
	mov bl,[auxiliarb]		; copia el byte que esta en "auxiliarb" en al
	add bl,'0'				; el valor de BL sera igual a la suma del ultimo valor de BL + codigo ASCII de '0'
	mov [msg+4],bl			; copia el registro EBX en el byte actual de msg con un desplazamiento de 4

	mov ebx,"fJ"			; almaceno la cadena "fJ" en codigo ASCII en EBX
	mov [msg+5], ebx		; copia el registro EBX en el byte actual de msg con un desplazamiento de 5

	imprimir msg,lenmsg		; llama a la macro imprimir con sus dos operandos

	ret						; retorna una subrutina extrayendo una direccion de la pila y salta a esa direccion
