%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro	
section .data
		msj db '*******',10
		len equ $-msj
section .text
		global _start

_start:
		mov ecx, 10

l1:
	push rcx
    imprimir msj,len
	
	pop rcx
	loop l1			; en este instante se decrementa cx en 1
	
	mov eax, 1
	int 80h