section .data
    saludo db 'Hola:'
    len_saludo equ $-saludo

    nlinea		db		10,10,0
	lnlinea		equ		$ - nlinea

section .bss
    item resb 2 ; reservame 5 espacios o caracteres
section .txt
    global _start

_start:
    mov cx,9

for:

    push rcx
    add rcx,'0'
    mov [item],rcx
    
    mov eax,4
    mov ebx,1
    mov ecx, saludo
    mov edx,len_saludo
    int 80h

    mov eax,4
    mov ebx,1
    mov ecx, item
    mov edx,1
    int 80h

    mov eax,4
    mov ebx,1
    mov ecx, nlinea
    mov edx,lnlinea
    int 80h

    pop rcx

    loop for
    pop rbx
    dec bx
    mov cx,9

    loop for
salir:
    mov eax,1
    int 80h