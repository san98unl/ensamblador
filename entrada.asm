section .data
    mensaje db 'Ingrese un numero',10
    len_mensaj equ $-mensaje
    mensaje_presentacion db 'El numero ingresado es',10
    len_mensaje_presentacion equ $-mensaje_presentacion

section .bss
    numero resb 5 ; reservame 5 espacios o caracteres

section .text
    global _start
_start:
    ;**********imprimir*****
    mov eax, 4
    mov ebx, 1
    mov ecx, mensaje
    mov edx, len_mensaj
    int 80h
    ;*******rescatar valos

    mov eax, 3 ; define el tipo de operacion
    mov ebx, 2 ; estandar de entrada
    mov ecx, numero ; lo que captura en el teclado
    mov edx, 5 ; debe coincidir con los numeros que reservamos 
    int 80h     ;interrupcion de linux

;******imprime mensaje_presentacion
    mov eax, 4
    mov ebx, 1
    mov ecx, mensaje_presentacion
    mov edx, len_mensaje_presentacion
    int 80h
;******imprime numero***
    mov eax, 4
    mov ebx, 1
    mov ecx, numero
    mov edx, 5
    int 80h
;
    mov eax,1
    int 80h