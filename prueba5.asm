;Examen de Ensamblador
;Por: Santiago Tuqueres
; 24-08-2020
%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro	
section .data
    asterisco db "*"
    
    new_line db " ", 10
    len_new_line equ $-new_line
section .bss

section .text
	global _start

_start:
    mov ax,1; filas
   

principal: ; para las filas 
   push rax    ; representa las filas 
   mov rcx,1;columnas
   jmp ciclo
 

ciclo:
    
    push rcx

    imprimir asterisco,1
    pop rcx
    inc rcx
    cmp cx,10
    jnz ciclo
    imprimir new_line,len_new_line

    pop rax
    inc rax
    cmp ax,10
    jnz principal
    
    


	mov eax,1
    int 80h
