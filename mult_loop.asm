; Multiplicacion de 2 numeros Estaticos, no se ingresan por teclado
; Por: Santiago Tuqueres
section .data
    resultado db "El resultado es:",10
    len equ $-resultado
    new_line db " ", 10
    len_new_line equ $-new_line
    

section .bss
    multiplicacion resb 1 ; reservame 5 espacios o caracteres
    

section .text
    global _start
_start:
;**********imprimir*****
    mov eax, 3      ; se representa en codigo ascii
    mov ebx, 2      ; se representa en codigo ascii
    mul ebx    
    add eax, '0'    ; ajuste
    mov [multiplicacion],eax

    mov eax, 4
    mov ebx, 1
    mov ecx, resultado
    mov edx, len
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, multiplicacion
    mov edx, 1
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, new_line
    mov edx, len_new_line
    int 80h

    mov eax,1
    int 80h