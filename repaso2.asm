; Deber tabla multilplicar del 1-9
; Por: Santiago Tuqueres
%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro	
section .data
    resultado db "El resultado es:",10
    msj1 db '*'
    len_msj1 equ $-msj1
    msj2 db '='
    len_msj2 equ $-msj2
    len equ $-resultado
    new_line db 10,''
    len_new_line equ $-new_line

    prueba db "yo"
    len_prueba equ $-prueba

    

section .bss
    multiplicacion resb 1 ; reservame 5 espacios o caracteres
    a resb 2
    b resb 2
    c resb 2

section .text
    global _start
_start:
     ;#contador de veces de la tabla
   


;**********primer y tercer operando*****
    ;#contador de veces de la tabla
    
   
    mov bx,9  ;filas 
    mov cx,9   ;columnas 
    
    ;push rcx
   
principal: ; para las filas 
    ;push rax   ; representa las filas 
    
    
    ;jmp ciclo
    ;*********
    push bx    ; representa las filas 
   cmp  bx,0
   jz salir
   jmp ciclo
ciclo:
    ;imprimir prueba, len_prueba
    ;pop rax
    ;inc rax
    ;cmp  rax,9
    ;jnz principal
    ;imprimir new_line,len_new_line
    
    
    ;************
    dec cx
    push cx
    imprimir prueba,len_prueba ;  el valor de ecx se remplasa 
    pop cx

    cmp cx,0
    jg ciclo      ;jg, verifica que el primer operando sea >(mayor) que el segundo
    imprimir new_line,len_new_line

    pop bx
    dec bx
    mov cx,9
    jmp principal ; salto sin condicion


salir:

    mov eax,1             
	int 80h