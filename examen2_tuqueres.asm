;Examen de Ensamblador 2
;Por: Santiago Tuqueres
; 26-08-2020
%macro escribir 2
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 0x80
%endmacro

segment .data
    arreglo db "santiago"
    len_arreglo equ $-arreglo

    msj2 db "S e encontro",10
    len2 equ $-msj2
	msj1 db "Ingresa caraxter", 10
	len_msj1 equ $-msj1
    msj3 db "Se leyo lo siguiente:", 10
	len_msj3 equ $-msj3

	archivo db "/home/san/Documentos/proyectos_ensamblador/resultados.txt"
    archivo_lec db "/home/san/Documentos/proyectos_ensamblador/datos.txt"

segment .bss
	texto resb 30
    carac resb 10
	idarchivo resd 1
    cont resb 2
    idarchivo2 resd 1
    aux resb 2

segment .text
	global _start

leer:
	mov eax, 3
	mov ebx, 0
	mov ecx, texto
	mov edx, 10
	int 80h
	ret
	
_start:
; ************************** abrir el archivo **************************
	mov eax, 5		;servicio para crear archivos, trabajar con archivos    ;operación de open
	mov ebx, archivo_lec	; dirección del archivo
	mov ecx, 0		
	mov edx, 777h
	int 80h
	
	test eax, eax		
	jz salir	
	
	mov dword [idarchivo2], eax

	escribir msj3, len_msj3
	
	; ************************** leer el archivo **************************
	mov eax, 3
	mov ebx, [idarchivo2]	
	mov ecx, texto		
	mov edx, 15
	int 80h
	
	escribir texto, 15
	
	; ************************** cerrar el archivo **************************
	mov eax, 6		; close
	mov ebx, [idarchivo2]	
	mov ecx, 0		
	mov edx, 0
	int 80h


    mov eax, 8 		;servicio para crear archivos, trabajar con archivos
	mov ebx, archivo	; dirección del archivo
	mov ecx, 1		; MODO DE ACCESO
					; O-RDONLY 0: El archivo se abre sólo para leer
					; O-WRONLY 1: El archivo se abre para escritura
					; O-RDWR 2: El archivo se abre para escritura y lectura
					; O-CREATE 256: Crea el archivo en caso que no exist
					; O-APPEND 2000h: El archivo se abro solo par escritura al final
	mov edx, 777h
	int 80h
	
	test eax, eax		; instrucción de comparación realiza la operación lógica “Y” de dos operandos, 
				; pero NO afecta a ninguno de ellos, SÓLO afecta al registro de estado. Admite 
				; todos los tipos de direccionamiento excepto los dos operandos en memoria
					; TEST reg, reg
					; TEST reg, mem
					; TEST mem, reg
					; TEST reg, inmediato
					; TEST mem, inmediato 
	
	jz salir		; se ejecuta cuando existen errores en el archivo
	
	mov dword [idarchivo], eax
	escribir msj1, len_msj1
	
	
	
	call leer
	
	; ************************ escritura en el archivo *****************************
	mov eax, 4
	mov ebx, [idarchivo]
	mov ecx, texto
	mov edx, 20
	int 0x80

    


    mov esi, texto;esi = fijar tamaño del arrreglo, posicionar el arreglo en memoria
    mov edi,0;edi = contener el indice del arreglo

    

    
    
    mov cx,[esi]
    mov [aux],cx
    
    
ciclo:
    
    mov al,[esi]
    ;mov al,[esi]
    push ax
    
    ;mov [esi],al
    ;sub al,'0'
    ;add al,2
    ;pop ax
    ;inc al
    ;add al,'0'
    ;mov [aux],al

    add esi,1
    add edi,1
    ;mov ax,[aux]
    ;escribir texto,2
   

    cmp [carac],ax; cmp 3,4 => activa carry
                        ; cmp 4,3 => desactiva carry y zero
                        ; cmp 3,3 => desactiva carry y zero se activa
    ;call comparar
    ;jne ciclo; se ejecurta cuando la bandera de carry esta activa
    jz contador
comparar:
    pop rbx
    cmp [carac], rbx
    jmp contador

contador:
    pop rcx
    inc rcx
    add rcx,'0'
    mov [cont],rcx
    escribir cont,1

    ;cmp edi,esi
    ;jne ciclo
    jmp salir


salir:
   
    ;escribir aux,1

	mov eax, 1
	int 80h
