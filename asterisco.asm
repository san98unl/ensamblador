;programa que imprime *****
;Por: Santiago Tuqueres
; 22-07-2020

section .data
    asterisco db '*'

section .text

    global _start
_start:
    ;queremos que se imprima 9 veces
    mov ecx, 9; al pasar a la impresion en la linea 12 se remplaza con asterisco por eso se usara pila
imprimir_asterisco:
    dec ecx;
    push cx;mando el valor 9 a la pila
    
    mov eax,4
    mov ebx,1
    mov ecx, asterisco
    mov edx,1
    int 80h

    pop cx
    ;jmp imprimir_asterisco
    cmp cx,0
    jnz imprimir_asterisco

    mov eax,1
    int 80h