;programa que imprime *****
;Por: Santiago Tuqueres
; 22-07-2020
%macro imprimir 1
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,1
    int 80h
%endmacro

section	.data
    msj db '*'
    new_line db 10,''

section	.text
   global _start 


_start:	
;tamano del cuadrado
    mov bx,9  ;filas 
    mov cx,9   ;columnas 

principal: ; para las filas 
   push bx    ; representa las filas 
   cmp  bx,0
   jz salir
   jmp asterisco



asterisco:
    dec cx
    push cx
    imprimir msj ;  el valor de ecx se remplasa 
    pop cx

    cmp cx,0
    jg asterisco       ;jg, verifica que el primer operando sea >(mayor) que el segundo
    imprimir new_line

    pop bx
    dec bx
    mov cx,9
    jmp principal ; salto sin condicion


salir:

    mov eax,1             ;system call number (sys_exit)
	int 0x80