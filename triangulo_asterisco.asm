section .data
		asterisco db '*'
		;leng equ $-asterisco
		nueva_linea db 10,'',10
		;len_nueva equ $-nueva_linea
section .text
		global _start

_start:
		mov ecx, 20
		mov ebx, 20
l1:
	;mov eax, 4
	;mov ebx, 1
	push rcx		;se envia la referencia de ecx a pila
	mov ebx, ecx
	push rbx
	call imprimir_enter
	;mov ecx, asterisco
	;mov edx, leng
	;int 80h
	
	pop rbx
	mov ecx, ebx
	push rbx
	
l2:
	push rcx		;se envia la referencia de ecx a pila
	call imprimir_asterisco
	pop rcx
	loop l2
	;***************************************
	pop rcx
	pop rbx
	mov ecx, ebx
	loop l1			; en este instante se decrementa cx en 1
	
	mov rax, 1
	int 80h

imprimir_asterisco:
	mov rax, 4
	mov ebx, 1
	mov ecx, asterisco
	mov edx, 1
	int 80h
	ret
	
imprimir_enter:
	mov eax, 4
	mov ebx, 1
	mov ecx, nueva_linea
	mov edx, 1
	int 80h
	ret