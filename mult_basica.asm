; Tabla del 3
; Por: Santiago Tuqueres
%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro	
section .data
    resultado db "El resultado es:",10
    msj1 db '*'
    len_msj1 equ $-msj1
    msj2 db '='
    len_msj2 equ $-msj2
    len equ $-resultado
    new_line db " ", 10
    len_new_line equ $-new_line
    

section .bss
    multiplicacion resb 1 ; reservame 5 espacios o caracteres
    a resb 2
    b resb 2
    c resb 2

section .text
    global _start
_start:

;**********primer y tercer operando*****
    mov al, 3 ; se representa en codigo ascii
    add al,'0'
    mov [a], al
    mov rcx,1
   



ciclo:
    
    push rcx
     ;#primer operando
    mov ax,[a]
    ;#segundi operandi
    add rcx,'0'
    mov [b],rcx
    ;#tercer operando operando
    sub rcx,'0'
    sub rax,'0'
    mul rcx
    add rax, '0'    ; ajuste
    
    mov [c],rax


    imprimir a,1
    imprimir msj1,len_msj1
    imprimir b,1
    imprimir msj2,len_msj2
    imprimir c,1
    imprimir new_line,len_new_line
    pop rcx
    inc rcx
    cmp cx,10; mayor a 10
    jnz ciclo;
    imprimir new_line,len_new_line


    mov eax,1
    int 80h