;ejercicio de mator numero con arreglos
section .data
    msj1 db "Ingrese 5 numeros",10
    len1 equ $-msj1 

    msj2 db "Numero mayor es",10
    len2 equ $-msj2

    arreglo db 0,0,0,0,0
    len_arreglo equ $-arreglo

    new_line db 10,''
    ;***Arhivo
    archivo db "/home/san/Documentos/proyectos_ensamblador/numeros.txt"

section .bss
    numero resb 2
    texto resb 30
	idarchivo resd 1
section .text
    global _start

_start:
    mov esi, arreglo
    mov edi,0

    ;************mensaje1*****
    mov eax,4
    mov ebx,1
    mov ecx,msj1
    mov edx,len1
    int 80h
    ;*******lectura de datos en arreglo****
leer:
    mov eax,3
    mov ebx,0
    mov ecx,numero
    mov edx,2
    int 80h

    mov al,[numero]
    sub al,'0'

    mov [esi],al;movemos el valor a un inice del arreglo

    inc edi
    inc esi ; indice del arreglo

    cmp edi,len_arreglo
    jb leer

    mov ecx,0
    mov bl,0

    

imprimir:
   

    mov al,[esi]
    add al,'0'

    mov [numero],al

    ;sub esi,1
    ;sub edi,1
    dec esi
    dec edi

    mov eax,4
    mov ebx,1
    mov ecx,numero
    mov edx,1
    int 80h

    

    cmp edi, 0; cmp 3,4 => activa carry
                        ; cmp 4,3 => desactiva carry y zero
                        ; cmp 3,3 => desactiva carry y zero se activa
    jge imprimir; se ejecurta cuando la bandera de carry esta activa
;
guardar:
    mov eax, 8 		;servicio para crear archivos, trabajar con archivos
	mov ebx, archivo	; dirección del archivo
	mov ecx, 1		; MODO DE ACCESO
					; O-RDONLY 0: El archivo se abre sólo para leer
					; O-WRONLY 1: El archivo se abre para escritura
					; O-RDWR 2: El archivo se abre para escritura y lectura
					; O-CREATE 256: Crea el archivo en caso que no exist
					; O-APPEND 2000h: El archivo se abro solo par escritura al final
	mov edx, 777h
	int 80h
	
	test eax, eax		; instrucción de comparación realiza la operación lógica “Y” de dos operandos, 
				; pero NO afecta a ninguno de ellos, SÓLO afecta al registro de estado. Admite 
				; todos los tipos de direccionamiento excepto los dos operandos en memoria
					; TEST reg, reg
					; TEST reg, mem
					; TEST mem, reg
					; TEST reg, inmediato
					; TEST mem, inmediato 
	
	jz salir		; se ejecuta cuando existen errores en el archivo
	
	mov dword [idarchivo], eax

    mov eax, 4
	mov ebx, [idarchivo]
	mov ecx, numero
	mov edx, 20
	int 0x80
salir:
    mov eax,1
    int 80h