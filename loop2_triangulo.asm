section .data
    asterisco db '*'
    nueva_linea db 10,''
section .text
    global _start

_start:
    mov rcx,20
    mov rbx,1

l1:
    push rcx
    push rbx
    ;*****Nueva linea *****8
    mov rax,4
    mov rbx,1
    mov rcx,new_line
    mov rdx,1
    int 80h

    pop rcx
    push rcx

l2:
    push rcx

     ;*****asterisco *****8
    mov rax,4
    mov rbx,1
    mov rcx,msj
    mov rdx,1
    int 80h
     
    pop rcx 
    loop l2

    ;****** final loop columnas

    pop rbx
    pop rcx
    inc rbx

    loop l1 

    mov eax,1
    int 80h

