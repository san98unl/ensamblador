;programa que imprime triangulo con loop
;Por: Santiago Tuqueres
; 29-07-2020
section .data
    asterisco db '*'

    new_enter db 10,''

section .txt
    global _start

_start:
    mov cx,9
    mov bx,9
    
    


    
inicio:
    push rcx
    
    
    mov eax,4
    mov ebx,1
    mov ecx, asterisco
    mov edx,1
    int 80h

    pop rcx

    loop inicio

    mov eax,4
    mov ebx,1
    mov ecx, new_enter
    mov edx,1
    int 80h

    
    ;push rcx
    mov cx,bx
    push rcx
    dec cx
    pop rcx
    
    loop inicio
    

    
salir:
    mov eax,1
    int 80h
