section .data
	nombres db "SantiagoTuqueres",10 ; constante mensaje de un byte en memoria
	len_nombres equ $ - nombres ; constante longitud que calcula el numero de caracteres

	edad db "21",10
	len_edad equ $ - edad

	materia db "Lenguaje Ensamblador",10
	len_materia equ $ - materia

	genero db "Masculino",10
	len_genero equ $ - genero

	
;section .bss

section .text
	global _start
_start:
	; ******************imprimir el mensaje****************************
	mov eax, 4 		; tipo de opearacion con la interrupcion =>escritura,salida
	mov ebx, 1 		; tipo de estandar, por teclado
	mov ecx, nombres 	; registro ecx se almacena la referencia a imprimir "mensaje
	mov edx, len_nombres 	; registro ecx se almacena la referencia a imprimir x #decaracteres
	int 80H 		; interrupcion de software linux

	mov eax, 4 
	mov ebx, 1
	mov ecx, edad
	mov edx, len_edad
	int 80H 

	mov eax, 4 
	mov ebx, 1
	mov ecx, materia
	mov edx, len_materia
	int 80H

	mov eax, 4 
	mov ebx, 1
	mov ecx, genero
	mov edx, len_genero
	int 80H 


	
	mov eax, 1		; salida del programa, siempre debemos definir, system_exit
	mov ebx, 0		; si el retorno es 0 (200 en la web) OK
	int 80H