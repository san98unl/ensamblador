%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro
segment .data

    msj1 db "S e encontro",10
    len1 equ $-msj1

    arreglo db 0,0,0,0
    len_arreglo equ $-arreglo
    new_line db 10,''

    ;***Arhivo
    archivo db "/home/san/Documentos/proyectos_ensamblador/s1.txt"
segment .bss
    numero resb 1
    idarchivo resd 1
segment .text
    global _start
_start:
    mov esi, arreglo;esi = fijar tamaño del arrreglo, posicionar el arreglo en memoria
    mov edi,0;edi = contener el indice del arreglo

    mov bx,1
    mov [numero],bx
    push rbx

    mov cx,len_arreglo
    push rcx
    
    
ciclo:
    
    mov al,[numero]
    ;mov al,[esi]
    push ax
    
    mov [esi],al
    sub al,'0'
    ;add al,2
    pop ax
    inc al
    add al,'0'
    mov [numero],al

    add esi,1
    add edi,1

    imprimir numero,2
    mov al,[numero]
   pop rbx

    cmp edi, len_arreglo; cmp 3,4 => activa carry
                        ; cmp 4,3 => desactiva carry y zero
                        ; cmp 3,3 => desactiva carry y zero se activa
    jne ciclo; se ejecurta cuando la bandera de carry esta activa
    jz salir    


    
    
salir:
    mov eax,1
    int 80h