%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro	
section .data
	resultado db "El resultado es:",10
    msj1 db '*'
    len_msj1 equ $-msj1
    msj2 db '='
    len_msj2 equ $-msj2
    len equ $-resultado
    new_line db " ", 10
    len_new_line equ $-new_line

section .bss
    multiplicacion resb 1 ; reservame 5 espacios o caracteres
    a resb 2
    b resb 2
    c resb 2
section .text
		global _start

_start:
	mov al, 9 ; se representa en codigo ascii
    
principal:
;**********primer y tercer operando*****
    ;#contador de veces de la tabla
    push rax 
    mov cx,ax
    add al,'0'
    mov [a], al
    mov rcx,9
    jmp ciclo
l1:
	push rcx
   mov ax,[a]
    ;#segundi operandi
    add rcx,'0'
    mov [b],rcx
    ;#tercer operando operando
    sub rcx,'0'
    sub rax,'0'
    mul rcx
    add rax, '0'    ; ajuste
    
    mov [c],rax


    call imprimir_a
    call imprimir_msj1
    call imprimir_b
    call imprimir_msj2
    call imprimir_c
    call imprimir_new_line
    
    pop rcx
    loop principal
    call imprimir_new_line

imprimir_a:
    mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,a 		; almacena el primer operando para imprimir
	mov edx,1		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H	
    ret

imprimir_msj1:
    mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,msj1		; almacena el primer operando para imprimir
	mov edx,len_msj1		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H	
    ret

imprimir_b:
    mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,b		; almacena el primer operando para imprimir
	mov edx,1		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H	
    ret
imprimir_new_line:
    mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,new_line		; almacena el primer operando para imprimir
	mov edx,len_new_line		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H	
    ret

imprimir_c:
    mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,c		; almacena el primer operando para imprimir
	mov edx,1		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H	
    ret

imprimir_msj2:
    mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,msj2		; almacena el primer operando para imprimir
	mov edx,len_msj2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H	
    ret


    mov eax,1
    int 80h