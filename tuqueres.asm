;Examen de Ensamblador
;Por: Santiago Tuqueres
; 24-08-

;Lo divido al valor con todos sus predecesores y si en mas de dos el residuo es 0 ; no es primo
%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro
%macro leer 1
	mov eax, 3 					;define el tipo de operacion
	mov	ebx, 2					;estandar de entrada
	mov ecx, %1		; lo que captura el teclado
	mov edx, 2					; numero de caracteres que se necesitanc on el enter
	int 80h
%endmacro
section .data
	msj db "Ingrese un número:"
	len equ $ - msj
	new_line db '',10

    mensaje db 'Ingrese primer numero ',10
	len_mensaje equ $-mensaje

    msj_resultado db "El resultado es:",10
    len_r equ $-msj_resultado

    resultado4 db "El resultado del cociente es:",10
    len4 equ $-resultado4

    resultado5 db "El resultado del residuo es:",10
    len5 equ $-resultado5

    resultado6 db "El resultado del contador es:",10
    len6 equ $-resultado6

    resultado7 db "Es primo",10
    len7 equ $-resultado7

    resultado8 db "No es primo",10
    len8 equ $-resultado8

section .bss
	numero resb 2
	resultado resb 1
    cont resb 2

    cociente_div resb 1 ; reservame 5 espacios o caracteres
    residuo_div resb 1 ; reservame 5 espacios o caracteres

    cociente_div2 resb 1 ; reservame 5 espacios o caracteres
    residuo_div2 resb 1 ; reservame 5 espacios o caracteres

    a resb 1
	
section .text
	global _start

_start :
	; ******* mensaje ingrese número
	mov eax, 4
	mov ebx, 1
	mov ecx, msj
	mov edx, len
	int 80h

	mov eax, 3
	mov ebx, 2
	mov ecx, numero
	mov edx, 1
	int 80h

   mov ax,[numero]
   mov [resultado],ax

   mov cx,0
   ;mov [cont],cx
   push rcx
    ;push rax
	jmp imp

imp:
    


	mov eax, 4
	mov ebx, 1
	mov ecx, numero
	mov edx, 1
	int 80h

	mov eax, 4
	mov ebx, 1
	mov ecx, new_line
	mov edx, 1
	int 80h

	mov eax, [numero]
	sub eax, '0'
	dec eax	
	mov ecx, eax
	add eax, '0'
	mov [numero], eax	

	
    cmp ecx, 0
	jz salir
	jmp div_a_1

div_a_1:
    ;pop rax
    ;sub al, '0'
    mov bl, [numero]
    mov al, [resultado]
    ;mov bh,
	;mov bh, [a]
    sub al, '0'
	sub bl, '0'     
	;sub bh, '0'
    ;push rax
    div bl  
    add al, '0'    ; ajuste
    mov [cociente_div],al
    
    
    mov [residuo_div],ah
    cmp ah,0
    jz condicion
    add ah, '0'
    ;call comparacion
    

    ;push rax;*****
    imprimir new_line,1
    imprimir resultado4, len4
    imprimir new_line,1
    imprimir cociente_div,1
    imprimir new_line,1
    imprimir resultado5, len5
    imprimir new_line,1
    imprimir residuo_div,1
    imprimir new_line,1
    imprimir resultado6, len6
    imprimir cont,1
    call comparacion
    
    ;mov bl,0

    jmp imp
    
condicion:
    call incremento
incremento:
    pop rcx
    inc rcx
    ret
    

comparacion:
    pop rcx
    ;inc rcx
    add  rcx, '0'
    mov [cont],rcx
    
    imprimir cont,1
    cmp rcx,2
    ja no_primo
    jz primo
    imprimir resultado7, len7
    ;imprimir cont,1
    ret

    
primo:
    ;call contador
    imprimir resultado7, len7
    imprimir new_line,1
no_primo:
  ;call contador
    imprimir resultado8, len8
    imprimir new_line,1

salir:
    
	mov eax, 1
	int 80h