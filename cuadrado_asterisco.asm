
%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro			; finaliza la instruccion repetitiva "imprimir"

section .data
    msj db '*'
    new_line db " ", 10
    len_new_line equ $-new_line

section .text
    global _start
_start:
    mov bx,9; filas
    mov cx,9; columnas

principal: ;se define para filas
    push bx
    cmp bx,0
    
    jz salir
    jmp asterisco

asterisco:
    dec cx
    push cx
    imprimir msj,1 ; el valor de ecx se remplaza con asterisco
    pop cx; si no rescato el valor se decrementa el asterisco
    cmp cx,0
    jg asterisco  ; cualdo el valor es menor del primer operando

    imprimir new_line, len_new_line
    
    pop bx
    dec bx
    mov cx,9
    jmp principal
salir:
    mov ax,1
    int 80h
