;Examen de Ensamblador
;Por: Santiago Tuqueres
; 24-08-2020
%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro	
%macro leer 1
	mov eax, 3 					;define el tipo de operacion
	mov	ebx, 2					;estandar de entrada
	mov ecx, %1		; lo que captura el teclado
	mov edx, 2					; numero de caracteres que se necesitanc on el enter
	int 80h
%endmacro
section .data
mensaje db 'Ingrese primer numero ',10
	len_mensaje equ $-mensaje
	
	mensaje2 db 'Ingrese segundo numero ',10
	len_mensaje2 equ $-mensaje2
	
    resultado db "El resultado es:",10
    len equ $-resultado
    new_line db " ", 10
    len_new_line equ $-new_line

    resultado2 db "El resultado de la resta es:",10
    len2 equ $-resultado2

    resultado3 db "El resultado de la multiplicacion es:",10
    len3 equ $-resultado3

    resultado4 db "El resultado del cociente es:",10
    len4 equ $-resultado4

    resultado5 db "El resultado del residuo es:",10
    len5 equ $-resultado5

    resultado6 db "El resultado de la suma es:",10
    len6 equ $-resultado6

section .bss
    a resb 1 
	b resb 1
	;resultado resb 1
    suma resb 1 ; reservame 5 espacios o caracteres
     
section .text
	global _start

_start:
    imprimir mensaje, len_mensaje
    leer a

    imprimir mensaje2, len_mensaje2
    leer b
;**********Suma************
    mov eax, [a]
	mov ebx, [b]
	sub eax, '0'     
	sub ebx, '0'
	
	add eax, ebx
	add eax, '0'           ; convierto a string
	
	mov [resultado], eax

    imprimir resultado6,len6

	imprimir resultado,1

    imprimir new_line, len_new_line
salir:
	mov eax,1
    	int 80h

