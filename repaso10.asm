%macro imprimir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro
segment .data

    msj1 db "S e encontro",10
    len1 equ $-msj1

    arreglo db "santiago"
    len_arreglo equ $-arreglo
    new_line db 10,''

    ;***Arhivo
    archivo db "/home/san/Documentos/proyectos_ensamblador/s1.txt"
segment .bss
    numero resb 1
    idarchivo resd 1
segment .text
    global _start
_start:
    mov esi, arreglo;esi = fijar tamaño del arrreglo, posicionar el arreglo en memoria
    mov edi,0;edi = contener el indice del arreglo

    mov bx,'s'
    push rbx

    mov cx,len_arreglo
    push rcx
ciclo:
    mov al,[esi]
    ;add al,'0'

    mov [numero],al

    add esi,1
    add edi,1

    

   pop rbx

    cmp [numero], bx; cmp 3,4 => activa carry
                        ; cmp 4,3 => desactiva carry y zero
                        ; cmp 3,3 => desactiva carry y zero se activa
    jne ciclo; se ejecurta cuando la bandera de carry esta activa
    jz final

final:
    imprimir msj1,len1
    mov eax, 8 		;servicio para crear archivos, trabajar con archivos
	mov ebx, archivo	; dirección del archivo
	mov ecx, 1		; MODO DE ACCESO
					; O-RDONLY 0: El archivo se abre sólo para leer
					; O-WRONLY 1: El archivo se abre para escritura
					; O-RDWR 2: El archivo se abre para escritura y lectura
					; O-CREATE 256: Crea el archivo en caso que no exist
					; O-APPEND 2000h: El archivo se abro solo par escritura al final
	mov edx, 777h
	int 80h
	
	test eax, eax		; instrucción de comparación realiza la operación lógica “Y” de dos operandos, 
				; pero NO afecta a ninguno de ellos, SÓLO afecta al registro de estado. Admite 
				; todos los tipos de direccionamiento excepto los dos operandos en memoria
					; TEST reg, reg
					; TEST reg, mem
					; TEST mem, reg
					; TEST reg, inmediato
					; TEST mem, inmediato 
	
	jz salir		; se ejecuta cuando existen errores en el archivo
	
	mov dword [idarchivo], eax
    
    repi:
        mov eax, 4
        mov ebx, [idarchivo]
        mov ecx, numero
        mov edx, 20
        int 0x80
        pop rcx
        loop repi
salir:
    mov eax,1
    int 80h