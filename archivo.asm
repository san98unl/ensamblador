
%macro escribir 2	; inicia una instruccion repetitiva denominada"imprimir" que se puede llamar en cualquier parte
	mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,1		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,%1 		; almacena el primer operando para imprimir
	mov edx,%2		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H			; interrupcion de software libre linux y da significado a las anteriores lineas
%endmacro	

segment .data
    msj1 db "ingresa datos en el archivi",10
    len_msj1 equ $-msj1

    archivo db "/home/san/Documentos/proyectos_ensamblador"

segment .bss
    texto resb 30
    idarchivo resd 1

segment .text
    global _start

_start:

    mov eax,8; servico ara crear archivos
    mov ebx,archivo; archivo direccion
    mov ecx,1   ;modp de acceso cuando es 0 es readonly
                ;cuando es 1 es wronly es solo escritura
                ;cuando es 2 se abre para escritura y lestura
                ;cuando es 256 crea el archivo en caso q no exista
                ;2000h se abre para escritura al final

    mov edx,777h    ; permisos
    int 80h

    test eax,eax
    jz salir    ; cuando existen erroes en el archivo

    escribir msj1,len_msj1

    mov dword[idarchivo],eax
    call leer

    ;********Escritura en el archivo
    mov eax,4		; indica el tipo de operacion en la cual sera de escritura y tendra una operacion de salida
	mov ebx,[idarchivo]		; mostrara el estandar de la aplicacion en este caso es por teclado
	mov ecx,texto 		; almacena el primer operando para imprimir
	mov edx,20		; almacena la longitud a imprimir en este caso el segundo operando
	int 80H	

leer:
    mov eax,3
    mov ebx,0
    mov ecx,texto
    mov edx,10
    int 80H
    ret
salir:
    mov eax,1
    int 80h
